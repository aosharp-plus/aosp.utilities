﻿using System;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Inventory;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Collections.Generic;

namespace ICE
{
    public class Main : AOPluginEntry
    {
        public static bool Enable = false;
        public static double _timer;

        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("ICE loaded!");
                Chat.WriteLine("/ICE to enable.");

                Game.OnUpdate += OnUpdate;

                Chat.RegisterCommand("ICE", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    Enable = !Enable;
                    Chat.WriteLine($"ICE : {Enable}");
                });
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void OnUpdate(object s, float deltaTime)
        {
            if (Enable)
            {
                var NanoProgrammingInterface = Inventory.Items.Where(c => c.Name == "Nano Programming Interface").FirstOrDefault();

                if (Time.NormalTime > _timer )
                {
                    List<Item> UpgradedControllerRecompilerUnit = Inventory.Items.FindAll((Item x) => x.Name == "Upgraded Controller Recompiler Unit");

                    if (UpgradedControllerRecompilerUnit?.Count > 1)
                    {
                        CharacterActionMessage characterActionMessage = new CharacterActionMessage();
                        characterActionMessage.Action = (CharacterActionType)53;
                        characterActionMessage.Target = UpgradedControllerRecompilerUnit[1].Slot;
                        Identity slot = UpgradedControllerRecompilerUnit[0].Slot;
                        characterActionMessage.Parameter1 = (int)slot.Type;
                        slot = UpgradedControllerRecompilerUnit[0].Slot;
                        characterActionMessage.Parameter2 = slot.Instance;
                        Network.Send(characterActionMessage);
                    }
                    else
                    {
                        foreach (var ice in Inventory.Items.Where(c => c.Name == "Hacker ICE-Breaker Source"))
                        {
                            NanoProgrammingInterface?.UseOn(ice.Slot);
                        }
                    }

                    _timer = Time.NormalTime +0.3;
                }
            }
        }

        public override void Teardown()
        {
        }
    }
}
