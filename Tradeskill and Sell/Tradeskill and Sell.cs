﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Linq;

namespace TradeSkillAndSell
{
    public class TradeSkillAndSell : AOPluginEntry
    {
        string ItemName;
        string ToolName;
        string SaleItem;

        int itemCount;

        double delay;

        State CurrentState = new State();
        State CheckState = new State();

        public override void Run()
        {
            base.Run();

            foreach (var bag in Inventory.Items.Where(b => b.UniqueIdentity.Type == IdentityType.Container))
            {
                bag.Use();
                bag.Use();
            }

            Network.N3MessageReceived += N3MessageReceived;
            Trade.TradeStateChanged += TradeStateChanged;

            Chat.RegisterCommand("itemname", (string command, string[] param, ChatWindow chatWindow) =>
            {
                ItemName = string.Join(" ", param.Skip(0));
                Chat.WriteLine($"Item Name = {string.Join(" ", param.Skip(0))}");
            });

            Chat.RegisterCommand("toolname", (string command, string[] param, ChatWindow chatWindow) =>
            {
                ToolName = string.Join(" ", param.Skip(0));
                Chat.WriteLine($"Tool Name = {string.Join(" ", param.Skip(0))}");
            });

            Chat.RegisterCommand("saleitem", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SaleItem = string.Join(" ", param.Skip(0));
                Chat.WriteLine($"Item to sale name = {string.Join(" ", param.Skip(0))}");
            });

            Chat.RegisterCommand("enable", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Chat.WriteLine($"Started");

                CurrentState = State.Open;
                Game.OnUpdate += OnUpdate;
            });

            Chat.RegisterCommand("disable", (string command, string[] param, ChatWindow chatWindow) =>
            {
                Chat.WriteLine($"Stopped");
                CurrentState = State.Stop;
                Game.OnUpdate -= OnUpdate;
            });

            if (!Game.IsNewEngine)
            {
                Chat.WriteLine("Tradeskill And Sell loaded! /enable to start, /disable to stop");
                Chat.WriteLine("/itemname name of item to ts");
                Chat.WriteLine("/toolname name of tool");
                Chat.WriteLine("/saleitem name of item to sell");
            }
            else
            {
                Chat.WriteLine("Does not work on this engine!");
            }
        }

        private void TradeStateChanged(Identity identity, TradeState state)
        {
            switch (state)
            {
                case TradeState.Opened:
                    if (CurrentState == State.WaitingOnVendingMachine) { CurrentState = State.Combine; }
                    break;
                case TradeState.Accept:

                    break;
            }
        }

        private void N3MessageReceived(object sender, N3Message e)
        {
            switch (e.N3MessageType)
            {
                case N3MessageType.Trade:
                    var tradeMsg = (TradeMessage)e;

                    if (tradeMsg.Action == TradeAction.AddItem)
                    {
                        if (itemCount >= 29)
                        {
                            Trade.Accept();
                            itemCount = 0;
                            CurrentState = State.Open;
                        }
                        else
                        {
                            CurrentState = State.Combine;
                        }

                        itemCount++;
                    }
                    break;
                case N3MessageType.InventoryUpdate:
                    Chat.WriteLine("InventoryUpdate");
                    break;
            }
        }

        private void OnUpdate(object sender, float e)
        {
            if (DynelManager.LocalPlayer.GetStat(Stat.Cash) == 999999000) 
            {
                CurrentState = State.Stop;
                Game.OnUpdate -= OnUpdate;
            }

            //if (CurrentState != CheckState)
            //{
            //    Chat.WriteLine(CurrentState);
            //    CheckState = CurrentState;
            //}

            switch (CurrentState)
            {
                case State.Open:
                    var vendingMachine = DynelManager.AllDynels.Where(vm => vm.Identity.Type == IdentityType.VendingMachine)
               .OrderBy(d => d.DistanceFrom(DynelManager.LocalPlayer)).FirstOrDefault(d => d.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 8);

                    vendingMachine.Use();
                    CurrentState = State.WaitingOnVendingMachine;
                    break;
                case State.MoveToTrade:

                    foreach (var item in Inventory.Items.Where(i => i.Name == SaleItem))
                    {
                        Trade.AddItem(item.Slot);
                    }

                    break;
                case State.Combine:

                    if (!Inventory.Items.Any(c => c.Name == ItemName))
                    {
                        foreach (var bag in Inventory.Backpacks)
                        {
                            foreach (var item in bag.Items)
                            {
                                if (item.Name == ItemName)
                                {
                                    item.MoveToInventory();
                                    return;
                                }
                            }
                        }
                    }
                    var source = Inventory.Items.FirstOrDefault(i => i.Name == ItemName);

                    if (!Inventory.Items.Any(c => c.Name == ToolName && c.Slot != source.Slot))
                    {
                        foreach (var bag in Inventory.Backpacks)
                        {
                            foreach (var item in bag.Items)
                            {
                                if (item.Name == ToolName)
                                {
                                    item.MoveToInventory();
                                    return;
                                }
                            }
                        }
                    }
                   
                    var target = Inventory.Items.FirstOrDefault(i => i.Name == ToolName && i.Slot != source.Slot);

                    if (source == null) { return; }
                    if (target == null) { return; }

                    if (target.Slot.Type != IdentityType.Inventory) { target.MoveToInventory(); }
                    if (source.Slot.Type == IdentityType.Backpack) { source.MoveToInventory(); }

                    Network.Send(new CharacterActionMessage
                    {
                        Action = CharacterActionType.UseItemOnItem,
                        Identity = DynelManager.LocalPlayer.Identity,
                        Target = target.Slot,
                        Parameter1 = (int)IdentityType.Inventory,
                        Parameter2 = source.Slot.Instance,
                    });

                    CurrentState = State.MoveToTrade;
                    break;
            }
        }

        private enum State
        {
            Open, WaitingOnVendingMachine, Combine, MoveToTrade, Waiting, Stop
        }
    }
}
